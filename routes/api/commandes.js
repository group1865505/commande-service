
const express = require('express');

const router = express.Router();
const Commande = require('../../model/commande');

  router.post('/', async (req, res) => {
         
    try {
      const { productId } = req.body;
  
      const nouvelleCommande = new Commande({
        productId,
      });

    const savedCommande = await nouvelleCommande.save();

    res.status(201).json(savedCommande);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Erreur lors de l\'ajout de la commande' });
  }
});

 router.get('/:id', async (req, res) => {
  try {
    const commandeId = req.params.id;

    const commande = await Commande.findById(commandeId);

    if (!commande) {
      throw new CommandeNotFoundException("Cette commande n'existe pas");
    }

    res.json(commande);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Erreur lors de la récupération de la commande" });
  }
});

  

router.put('/:id', async (req, res) => {
    try {
      const commandeId = req.params.id;
      /*const { commandePayee } = req.body;*/
  
      const updatedCommande = await Commande.findByIdAndUpdate(commandeId, { commandePayee: true }, { new: true });

      if (updatedCommande) {
        res.sendStatus(200);
      } else {
        res.status(404).json({ message: "Commande introuvable" });
      }
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: "Erreur lors de la mise à jour de la commande" });
    }
  });
router.get('/', async (req, res) => {
    try {
      const commandes = await Commande.find();
      res.json(commandes);
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Erreur lors de la récupération des commandes' });
    }
  });
  
  


module.exports = router ; 
