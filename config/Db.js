const mongoose = require('mongoose');
require('dotenv').config();

const CommandeDB = async () => {

  try {
    await mongoose.connect(process.env.MONGODB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    console.log('Connected to CommandeDB');
  } catch (error) {
    console.error('Error connecting to CommandeDB:', error);
  }
};


module.exports = CommandeDB;