const mongoose = require('mongoose');

const commandeSchema = new mongoose.Schema({
  productId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true
  },
  dateCommande: {
    type: Date,
    default: Date.now
  },
  commandePayee: {
    type: Boolean,
    default: false
  }
});

const Commande = mongoose.model('Commande', commandeSchema);

module.exports = Commande;
