const express = require('express');
const bodyParser = require('body-parser');
const commandes = require('./routes/api/commandes');
const app = express();

const cors = require('cors');
const corsOptions = {
  origin: '*' ,
  credentials: true,
  'allowedHeaders': ['sessionId', 'Content-type'],
  'exposedHeaders': ['sessionId'],
  'methods': 'GET, HEAD, PUT, PATCH, POST, DELETE',
  'preflightContinue': false
}
app.use(cors(corsOptions));

const CommandeDB = require('./config/Db');


CommandeDB();

app.use(bodyParser.json());

app.use('/api/commandes' , commandes);


const port = process.env.PORT || 5000 ;

app.listen(port , ()=> console.log(`Le Microservice Commande est démaré sur le port ${port}`));